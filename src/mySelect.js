import React from "react";
import data from "./data";
import {Arc, Group, Layer, Rect, Stage, Text} from 'react-konva';

const COLORS = {
  pb_static: 'red',
  lifting: 'green',
  twisting: 'blue',
  intensity: 'yellow',
  repetition: 'white'
}
const getBarProps = (day) => {
  return Object.keys(COLORS).reduce((ac, key) => {
    ac.total += day[key];
    ac[key] = {
      value: day[key],
      y: ac.total,
      color: COLORS[key],
      name: key
    };
    return ac;
  }, {total: 0});
}
const getOverall = (item) => {
  return Object.keys(COLORS).reduce((ac, key) => {
    let value = item[key] ? (360 / (item.overallHazard / item[key])) : 0;
    ac.arr.push({
      value,
      color: COLORS[key],
      name: key,
      rotate: ac.prev
    });
    ac.prev += value;
    return ac;
  }, {arr: [], prev: 0});
}


class MySelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {day: data[0], overallHazards: getOverall(data[0].overallHazards)};
    this.changeDay = (e) => {
      this.setState({day: data[e.target.value], overallHazards: getOverall(data[e.target.value].overallHazards)});
      console.log(getOverall(data[e.target.value].overallHazards))
      console.log(data[e.target.value].overallHazards)
    }
  }

  render() {
    return (
      <div className="kuku">
        <select name="day" id="dates" onChange={this.changeDay} value={this.state.index}>
          {data.map((day, i) => (
            <option value={i} key={i}>{new Date(day.timeStart).toLocaleString()}</option>
          ))}
        </select>

        <Stage width={window.innerWidth} height={window.innerHeight}>
          <Layer>
            {Object.keys(COLORS).map((item, i) => {
              return (
                <Group key={item}>
                  <Rect x={80} y={500 + 30 * i} fill={COLORS[item]} stroke={COLORS[item]} width={25} height={25}/>
                  <Text text={item} x={120} y={510 + 30 * i} fill="white"/>
                </Group>
              )
            })}
            {Object.values(this.state.day.hazardsByHourList).map((hour, i) => {
              return Object.values(getBarProps(hour)).filter(item => item.color).map((item) => (
                <Group key={item.name}>
                  <Rect x={80 + 80 * i} y={400 - 10 * item.y} width={30} height={10 * item.value} fill={item.color}
                        stroke={item.color}/>
                  <Text text={`${new Date(hour.timeInMilliseconds).getHours()}:00`} fontSize={15} x={80 + 80 * i}
                        y={430} fill="white"/>
                </Group>
              ))
            })}
            {
              this.state.overallHazards.arr.map((item, i) => {
                return (
                  <Arc x={550} y={550}
                       innerRadius={40}
                       outerRadius={70}
                       angle={item.value}
                       fill={item.color}
                       stroke={item.color}
                       rotation={item.rotate}
                       key={i}/>
                )
              })}
          </Layer>
        </Stage>
      </div>
    );
  }
}

export default MySelect;
