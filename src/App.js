import './App.css';
import MySelect from "./mySelect";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MySelect></MySelect>
      </header>
    </div>
  );
}

export default App;
